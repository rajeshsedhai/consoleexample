﻿using System;

namespace ConsoleBlogExample
{
    public class Abstract
    {
        public /*static*/ void Main(string[] args)
        {
            var objProduct = new Product();
            objProduct.GetAllProduct();
        }

    }
    public class ProductionCountry
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
    }

    public abstract class Laptop
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
    }


    public class Product : Laptop
    {
        public void GetAllProduct()
        {
            var objProductionCountry = new ProductionCountry()    // Non-abstract class can be instantiated. Can access properties of class ProductionCountry through instantiation.
            {
                Id = 1001,
                CountryName = "USA"
            };
            Console.WriteLine("-------Instantiated by Production Country-------");
            Console.WriteLine("Id:{0}", objProductionCountry.Id);
            Console.WriteLine("CountryName:{0}", objProductionCountry.CountryName);
            Console.WriteLine("");


            //var objLaptop = new Laptop() // Build Error. Abstract Cannot be instantiate since Laptop is abstract class.
            //{
            //    BrandId = 1,
            //    BrandName = "Dell"
            //};
            Console.WriteLine("-------Inherited from Laptop Class-------");
            BrandId = 1;
            BrandName = "Dell";  //Direct Access. Since abstract class Laptop is inherited , Product can use the property defined in abstract class Laptop.
            Console.WriteLine("BrandId:{0}", BrandId);
            Console.WriteLine("BrandName:{0}", BrandName);
            Console.WriteLine("");

        }

    }

}
