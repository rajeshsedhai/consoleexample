﻿using System;

namespace ConsoleBlogExample
{
    public class NullChecking
    {
        public /*static*/ void Main()
        {
            nullchecking();
        }

        public static void nullchecking()
        {
            string someValue = null;
            if (someValue != null) //It checks for null
            {
                if (someValue.Length == 0)
                {
                    Console.WriteLine(someValue);
                }
            }

        }
    }
}