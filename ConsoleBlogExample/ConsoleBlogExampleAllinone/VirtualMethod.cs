﻿using System;

namespace ConsoleBlogExample

{
    public class VirtualMethod
    {
        public /*static*/ void Main(string[] args)
        {
            var objBaseClass = new BaseClass();
            objBaseClass.SayHello();
            objBaseClass.SayGoodbye();

            var objDerivedClass = new DerivedClass();
            objDerivedClass.SayHello();
            objDerivedClass.SayGoodbye();
        }
    }
    public class BaseClass
    {

        public void SayHello()
        {
            Console.WriteLine("------Base Class------");
            Console.WriteLine("Hello");
        }


        public virtual void SayGoodbye()
        {
            Console.WriteLine("Goodbye");
            Console.WriteLine("");
        }

    }


    public class DerivedClass : BaseClass
    {
        public new void SayHello()
        {
            Console.WriteLine("------Derived Class------");
            Console.WriteLine("Hi There");
        }


        public override void SayGoodbye()
        {
            Console.WriteLine("Goodbye");
            Console.WriteLine("See you later"); //Added this line which is not in base class virtual method 'SayGoodbye'
            Console.WriteLine();

        }
    }

}
