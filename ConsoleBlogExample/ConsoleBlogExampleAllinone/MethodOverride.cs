﻿using System;


namespace ConsoleBlogExampleAllinone
{
    public class MethodOverride
    {
        public /*static*/ void Main()
        {
            var objPerson = new Person();
            objPerson.GetInfo();
            var objEmployee = new Employee();
            objEmployee.GetInfo();
        }
    }
    public class Person
    {
        protected string ppn = "0653978";
        protected string name = "Rajesh Sedhai";

        public virtual void GetInfo()
        {
            Console.WriteLine("Name: {0}", name);
            Console.WriteLine("PPN: {0}", ppn);
            Console.WriteLine("");
        }
    }
    class Employee : Person
    {
        public int id = 91914;
        public override void GetInfo()
        {
            // Calling the base class GetInfo method:
            base.GetInfo();
            Console.WriteLine("----Overriding from GetInfo method----");
            Console.WriteLine("Employee ID: {0}", id);
            Console.WriteLine("");
        }

    }

}