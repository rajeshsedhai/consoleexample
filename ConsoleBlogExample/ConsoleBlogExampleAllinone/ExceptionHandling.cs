﻿using System;

namespace ConsoleBlogExampleOP
{
    public class ExceptionHandling
    {
        public /*static*/ void Main(string[] args)
        {
            WithoutException();
            WithException();
        }

        public static void WithoutException()
        {
            int a = 2;
            int b = 10;
            Console.WriteLine("---Without Exception---");
            try
            {
                b = 0 / a;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Please try again later. Error: " + ex.Message);                
            }
            finally
            {
                Console.WriteLine("The value of b is:  " + b); // Will print 0 if no exception, print 10 if exception.                
            }
        }

        public static void WithException()
        {
            Console.WriteLine("---With Exception---");
            string a = "abc";
            int b = 10;
            try
            {
                b = Convert.ToInt32(a) / b;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Please try again later. Error: " + ex.Message); 
            }
            finally
            {
                Console.WriteLine("The value of b is:  " + b); // Will print 0 if no exception, print 10 if exception.
            }
        }
    }
}
