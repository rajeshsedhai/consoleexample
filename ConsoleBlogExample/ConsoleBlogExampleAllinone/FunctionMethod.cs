﻿//function main in C
//#include<stdio.h>

//void main()
//{
//    int x, y, z;
//    x = 10;
//    y = 15;
//    z = x + y;
//    printf("The sum value of z is %d", z);
//}
using System;

namespace ConsoleBlogExample
{    
    //method sum in C#
    public class FunctionMethod
    {
        public /*static*/ void Main()
        {
            var objmethod = new Method();
            objmethod.sum();            
        }
    }
    public class Method
        {
            int x, y, z;
            public void sum()
            {
                x = 10;
                y = 15;
                z = x + y;
                Console.WriteLine("The sum value of z is {0} ", z);
            }
        }
    }

