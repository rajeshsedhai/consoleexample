﻿using System;

namespace ConsoleBlogExample
{
    public class NullReference
    {
        public /*static*/ void Main()
        {
            Withoutnullchecking();
        }

        //Example of null reference exception
        public static void Withoutnullchecking()
        {
            string someValue = null;
            if (someValue.Length == 0) // It will Causes exception since there is a length property from null value 
            {
                Console.WriteLine(someValue); // <-- Unreachable code
            }
        }


    }
}