﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlogExample
{
    public class AsyncAndWaitReverse
    {
        public /*static*/ void Main(string[] args)
        {
            callMethod();
            Console.ReadKey();
        }

        public static async void callMethod()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Task<int> task = Method1();
            Method2();
            int count = await task;
            Method3(count);
            stopwatch.Stop();
            Console.WriteLine("Time elapsed: {0:hh\\:mm\\:ss}", stopwatch.Elapsed);
            //Console.WriteLine("Time elapsed:{0}", stopwatch.Elapsed);
        }
        public static async Task<int> Method1()
        {
            int count = 0;
            await Task.Run(() =>
            {
                for (int i = 0; i < 20000; i++)
                {
                    Console.WriteLine(" Method 1");
                    count += 1;
                }
            });
            return count;
        }


        public static void Method2()
        {
            for (int i = 0; i < 10000; i++)
            {
                Console.WriteLine(" Method 2");
            }
        }


        public static void Method3(int count)
        {
            Console.WriteLine("Total count is " + count);
        }
    }
}