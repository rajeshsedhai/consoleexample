﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlogExampleAllinone
{
    class GarbageCollection
    {
        static void Main()
        {
            {
        long bytes1 = GC.GetTotalMemory(false); // Get memory in bytes
        long bytes4 = GC.GetTotalMemory(true);
        byte[] memory = new byte[1000 * 1000 * 10]; // Ten million bytes
        memory[0] = 1; // Set memory (prevent allocation from being optimized out)

        long bytes2 = GC.GetTotalMemory(false); // Get memory
        long bytes3 = GC.GetTotalMemory(true); // Get memory

        Console.WriteLine(bytes1);
        Console.WriteLine(bytes2);
        Console.WriteLine(bytes2 - bytes1); // Write difference
        Console.WriteLine(bytes3);
        Console.WriteLine(bytes3 - bytes2); // Write difference
        Console.WriteLine(bytes4);
        Console.ReadLine();
    }
            //long mem1 = GC.GetTotalMemory(false);
            //{
                
            //    // Allocate an array and make it unreachable.
            //    int[] values = new int[43534534];
            //    //values = null;
            //}
            //long mem2 = GC.GetTotalMemory(false);
            //long mem3 = GC.GetTotalMemory(true);
            ////{
            ////    // Collect garbage.
            ////    GC.Collect();
            ////}
            ////long mem4 = GC.GetTotalMemory(false);
            //{
            //    Console.WriteLine("Program start at:"+mem1);
            //    Console.WriteLine("After assigning integer array:",mem2);
            //    Console.WriteLine("difference:",mem2-mem1);
            //    Console.WriteLine("After Garbag collection",mem3);
            //    Console.ReadLine();
            //}
        }
    }
}

