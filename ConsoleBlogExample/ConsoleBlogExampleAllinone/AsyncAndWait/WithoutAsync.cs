﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlogExample
{
    public class WithoutAsync
    {
        public /*static*/ void Main(string[] args)
        {
            callMethod();
            //Console.ReadKey();
        }

        public static /*async*/ void callMethod()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Method1();
            Method2();
            //int count = task;
            Method3();
            stopwatch.Stop();
            Console.WriteLine("Time elapsed: {0:hh\\:mm\\:ss}", stopwatch.Elapsed);
            //Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);
        }

        public static void Method1()
        {
            

            for (int i = 0; i < 20000; i++)
            {
                Console.WriteLine(" Method 1");
               
            }
            
        }

        public static void Method2()
        {
            for (int i = 0; i < 10000; i++)
            {
                Console.WriteLine(" Method 2");
            }
        }

        public static void Method3()
        {
            int count = 4;            
            Console.WriteLine("Total count is " + count);
        }
    }
}