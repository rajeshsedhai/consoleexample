﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsoleBlogExample
{
    public  class ProductionCountry
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
    }


    public abstract class Laptop
    {
        public int BrandId { get; set; }            //Property:- BrandId, BrandName
        public string BrandName { get; set; }
    }


    public class Product : Laptop
    {
        public void GetAllProduct() //method
        {
            var objProductionCountry = new ProductionCountry()    // Non-abstract class can be instantiated. Can access properties of class ProductionCountry through instantiation.
            {
                Id = 1001,                                         // object:- objProductionCountry, objLaptop
                CountryName = "USA"
            };
            Console.WriteLine("Id:{0}+{1}", objProductionCountry.Id, 2);
            Console.WriteLine("CountryName:{0}", objProductionCountry.CountryName);


            //var objLaptop = new Laptop() // Build Error. Abstract Cannot be instantiate since Laptop is abstract class.
            //{
            //    BrandId = 1,
            //    BrandName = "Dell"
            //};
            BrandId = 1;
            BrandName = "Dell";  //Direct Access. Since abstract class Laptop is inherited , Product1 can use the property defined in abstract class Laptop.
            Console.WriteLine("BrandId:{0}", BrandId);
            Console.WriteLine("BrandName:{0}", BrandName);

        }




    }

}