﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlogExample
{

    //Exceptions are a type of error that occurs during the execution of a program which are typically problems that are not expected. 
    //C# exception handling is built upon four keywords: try, catch, finally, and throw.
    //try − A try block identifies a block of code for which particular exceptions is activated.There can be one or more catch blocks.
    //catch − The catch keyword indicates the catching of an exception.
    //finally − The finally block is used to execute a given set of statements, whether an exception is thrown or not thrown.
    //throw − A program throws an exception when a problem shows up. This is done using a throw keyword.
    public class ExceptionHandling
    {
        public void WithoutException()
        {
            int a = 2;
            int b = 10;
            try
            {
                b = 0 / a;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Please try again later. Error: " + ex.Message);
                Console.ReadLine();
            }
            finally
            {
                Console.WriteLine("The value of b is:  " + b); // Will print 0 if no exception, print 10 if exception.
                Console.ReadLine();
            }
        }


        public void WithException()
        {
            int a = 2;
            int b = 10;
            try
            {
                b = a / 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Please try again later. Error: " + ex.Message);
                Console.ReadLine();
            }
            finally
            {
                Console.WriteLine("The value of b is:  " +b); // Will print 0 if no exception, print 10 if exception.
                Console.ReadLine();
            }
        }




    }
}
