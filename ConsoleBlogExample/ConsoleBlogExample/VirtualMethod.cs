﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//A virtual method is a method that can be redefined in derived classes. 
//Virtual method is used when a method's basic functionality is the same but sometimes more functionality is needed in the derived class.
//it is optional for the derived class to override that method.
//By default, methods are non-virtual. We can't override a non-virtual method.
//We can't use the virtual modifier with the static, abstract, private or override modifiers.
namespace ConsoleBlogExample
{
    public class BaseClass
    {
        public void SayHello()
        {
            Console.WriteLine("Hello");
            //Console.ReadLine();
        }


        public virtual void SayGoodbye()
        {
            Console.WriteLine("Goodbye");
            //Console.ReadLine();
        }

    }


    public class DerivedClass : BaseClass
    {
        public new void SayHello()
        {
            Console.WriteLine("Hi There");
            //Console.ReadLine();
        }


        public override void SayGoodbye()
        {
            Console.WriteLine("Goodbye");
            Console.WriteLine("See you later"); //Added this line which is not in base class virtual method 'SayGoodbye'
            //Console.ReadLine();
        }
    }


}