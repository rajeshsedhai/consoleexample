﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlogExample
{
    class Program
    {
        static void Main(string[] args)
        {
            //VirtualMethod();
            //Abstract();
            //MethodOverride();
            //StackAndHeap();
            //LazyLoadingAndEarlyLoading();
            //Exception();
            //Operator();
            AsyncAndAwait();
        }


        public static void AsyncAndAwait()
        {
            var obj = new test();
            test.callMethod();

        }
        /// <summary>
        /// Virtual metod example
        /// </summary>
        public static void VirtualMethod()
        {
            Console.WriteLine("VirtualMethod");
            var objBase = new BaseClass();
            objBase.SayHello();
            objBase.SayGoodbye();

            var objDerived = new DerivedClass();
            objDerived.SayHello();
            objDerived.SayGoodbye();
            Console.WriteLine("----------------------------------");
        }

        public static void Abstract()
        {
            Console.WriteLine("----------------------------------");
            Console.WriteLine("Abstract");
            var objNonAbstractProduct = new Product();
            objNonAbstractProduct.GetAllProduct();

            //var objAbstractProduct = new Product();
            //objAbstractProduct.GetAllProduct();
            //Console.WriteLine("----------------------------------");
        }

        public static void MethodOverride()
        {
            Console.WriteLine("----------------------------------");
            Console.WriteLine("Method Override");
            var objPerson = new Person();
            objPerson.GetInfo();

            var objEmployee = new Employee();
            objEmployee.GetInfo();
            Console.WriteLine("----------------------------------");
        }

        public static void StackAndHeap()
        {
            var objStackAndHeap = new StackAndHeap();
            objStackAndHeap.Addition();
        }

        public static void Exception()
        {
            var objexceptionhandling = new ExceptionHandling();
            objexceptionhandling.WithoutException();
            objexceptionhandling.WithException();
        }

        public static void LazyLoadingAndEarlyLoading()
        {

        }

        public static void Operator()
        {
            var temp = 200;
            int value = temp == 200 ? 100 : 200;
            Console.WriteLine(value);

            temp = 400;
            value = temp == 200 ? 100 : 200;
            Console.WriteLine(value);

            var temp1 = (dynamic)null;
            value = temp1 ?? 200; //The ?? operator is called the null-coalescing operator. 
            //It returns the left-hand operand if the operand is not null; otherwise it returns the right hand operand.
            Console.WriteLine(value); 

            temp1 = 16;
            value = temp1 ?? 200;
            Console.WriteLine(value);
            Console.ReadLine();
        }
    }

    public class test
    {
        public static async void callMethod()
        {
            Task<int> task = Method1();
            Method2();
            int count = await task;
            Method3(count);
        }

        public static async Task<int> Method1()
        {
            int count = 0;
            await Task.Run(() =>
            {
                for (int i = 0; i < 100; i++)
                {
                    Console.WriteLine(" Method 1");
                    count += 1;
                }
            });
            return count;
        }


        public static void Method2()
        {
            for (int i = 0; i < 25; i++)
            {
                Console.WriteLine(" Method 2");
            }
        }


        public static void Method3(int count)
        {
            Console.WriteLine("Total count is " + count);
        }   
}


}
