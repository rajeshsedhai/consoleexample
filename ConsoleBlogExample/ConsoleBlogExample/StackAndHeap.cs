﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlogExample
{
    public class StackAndHeap
    {

        //    A Reference Type always goes on the Heap
        public void Addition()
        {
            int i = 2;     // When this line is executed, the compiler allocates a small amount of memory in the stack
            int j = 6;     // When this line is executed,,it stacks this memory allocation on top of the above memory allocation

            //It does not allocate memory for  an instance of StackandHeapExample. It only allocates a stack variable myClass assigning it null.
            //The time it hits the new keyword , it allocates on heap
            var myClass = new StackandHeapExample();  //When this line is executed it creates a pointer on the stack and the actual object is stored in a different type of memory location called ‘Heap’. ‘Heap’ does not track running memory. Heap is used for Dynamic Memory allocation
            
        } //When it passes the end control, it clears all the memory variables which are assigned on stack.
          //In other words all variables which are related to int data type are de-allocated in ‘LIFO’ fashion from the stack.
          //It did not de-allocate the heap memory.This memory will be later de-allocated by the garbage collector.
    }

    public class StackandHeapExample
    {

    }
}
