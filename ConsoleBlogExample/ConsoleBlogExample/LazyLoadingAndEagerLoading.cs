﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBlogExample
{
    class LazyLoadingAndEagerLoading
    {      
            //for loading the related entities of an entity


            //Eager Loading helps you to load all your needed entities at once. i.e.related objects (child objects) are loaded automatically with its parent object.
            //We use 'include' for eager loading.
            //var usr = dbContext.Users.Include(a => a.Address).FirstOrDefault();
            //var userCode = usr.UserCode;
            //var userName = usr.UserName;
            //var fullName = usr.FullName;
            //var temporaryAddress = usr.Addresses.TemporaryAddress;
            //var permanentAddress = usr.Addresses.PermanentAddress;

            //Equivalent SQL
            //SELECT u.UserCode,u.UserName,u.FulName,a. TemporaryAddress,a.PermanentAddress FROM USER LEFT JOIN Addresses ON a.UserId = u.Id;


            //When to use:
            //Use Eager Loading when the relations are not too much. Thus, Eager Loading is a good practice to reduce further queries on the Server.
            //Use Eager Loading when you are sure that you will be using related entities with the main entity everywhere



            // In case of lazy loading/Deferred loading, related objects (child objects) are not loaded automatically with its parent object until they are requested. 
            //var usr = dbContext.Users.FirstOrDefault();
            //var userCode = usr.UserCode;
            //var userName = usr.UserName;
            //var fullName = usr.FullName;
            //var temporaryAddress = usr.Addresses.TemporaryAddress;
            //var permanentAddress = usr.Addresses.PermanentAddress;

            //Equivalent SQL
            //SELECT u.UserCode, u.UserName, u.FulName FROM USER u
            //GO
            //SELECT a.TemporaryAddress, a.PermanentAddress FROM ADDRESS a WHERE a.UserId = u.Id;
            //GO

            //When to use:
            //Use Lazy Loading when you are using one - to - many collections.
            //Use Lazy Loading when you are sure that you are not using related entities instantly.
        }
    }
